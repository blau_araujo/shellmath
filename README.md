# shellmath

Uma coleção de funções matemáticas e aritméticas para o Bash usando apenas bash e awk.

Este é um projeto didático proposto para os alunos do [Curso Básico de Programação em Bash](https://cursos.debxp.org) e membros da [comunidade debxp](https://debxp.org).

## Uso

A ideia é que o arquivo `shellmath` seja inserido no '.bashrc' do usuário, tornando as funções disponíveis na linha de comando, ou inserido por `source` em scripts.

## Desafios iniciais

* [x] **1. calc** - Efetua expressões aritméticas
* [x] **2. maxvalb** - Calcula o maior inteiro possível dado o número de bytes
* [x] **3. isprime** - retorna sucesso se o número for primo (@robsonalexandre)
* [x] **4. isnumber** - verifica se a string é numérica (@robsonalexandre)
* [x] **5. isint** - verifica se a string expressa um inteiro (@robsonalexandre)
* [x] **6. isdoub** - verifica se a string expressa um número de ponto flutuante válido
* [x] **7. isfib** - verifica se o inteiro pertence à série de fibonacci (@robsonalexandre)
* [x] **8. isbin** - verifica se o número é um binário válido
* [x] **9. ishex** - verifica se o número é um hexadecimal válido (@robsonalexandre)
* [x] **10. round** - Arredonda números de ponto flutuante
* [x] **11. ceil** - Arredonda números de ponto flutuante para cima
* [x] **12. floor** - Arredonda números de ponto flutuante para baixo
* [ ] **13. isoct** - verifica se o número é um octal válido

~~**bconv** [bodh] NUM - converte NUM para a base em $1~~ [Resolvido com este script (bcv)](https://gitlab.com/blau_araujo/sistemas-logicos/-/blob/master/02-conversor-de-bases-em-bash/bcv)

